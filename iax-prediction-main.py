from darkflow.net.build import TFNet
import cv2
import time
import sys
import numpy as np


class predict:

    def __init__(self):

        self.classes = {
                        'train':'#88eac8',
                        'stop sign':'#fc1d14',
                        'car':'#4f5885',
                        'traffic light':'#fc1d14',
                        'bus':'#68c067',
                        'truck':'#68c067',
                        'person':'#ffffff',
                        'bicycle':'#68c067',
                        'bird':'#887b0d',
                        'cat':'#887b0d',
                        'dog':'#887b0d',
                        'horse':'#887b0d',
                        'sheep':'#887b0d',
                        'cow':'#887b0d'
                        }
        self.options = {"pbLoad": "built_graph/yolo.pb", "metaLoad": "built_graph/yolo.meta", "threshold": 0.3, "gpu": 0.8}
        self.tfnet = TFNet(self.options)
        self.cap = cv2.VideoCapture(sys.argv[1])


    def grab_frame(self):

        _, frame = self.cap.read()
        return cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    def run(self):
        obj = predict()

        c = 1
        while True:
            frame = obj.grab_frame()
            start = time.time()
            resultJSON = self.tfnet.return_predict(obj.grab_frame())
            end = time.time()
            print("Time taken : {0} seconds".format(end - start))

            # Detect Objects
            for x in resultJSON:
                label = x['label']
                x1 = x['topleft']['x']
                y1 = x['topleft']['y']
                x2 = x['bottomright']['x']
                y2 = x['bottomright']['y']
                obj.save_png(x1, x2, y1, y2, frame, c, label)
            c += 1
    def save_png(self, x1, x2, y1, y2, frame, c, label):
        out = cv2.rectangle(frame, (x1,y1), (x2,y2), (0, 255, 0), 2)
        pts = np.array([[155, 1078], [816, 600], [1078, 596], [1916, 1075]], np.int32)
        pts = pts.reshape((-1, 1, 2))
        out = cv2.polylines(out, [pts], True, (0, 255, 255), 2)
        out = cv2.putText(out, label, (x2 + 10, y2), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 255, 0))
        out = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        cv2.imwrite('output5/prediction'+str(c)+'.png', out)
if __name__ == "__main__":

    obj = predict()
    obj.run()