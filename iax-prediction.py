 #  ./flow --model cfg/tiny-yolo.cfg --load bin/yolo-tiny.weights --train --dataset "pascal/VOC2007/JPEGImages" --annotation "pascal/VOC2007/Annotations" --savepb --gpu 0.8
# ./flow --model https://www.floydhub.com/ishan39/datasets/yolofiles/1/tiny-yolo.cfg --load https://www.floydhub.com/ishan39/datasets/yolofiles/1/yolo-tiny.weights --train --dataset "https://www.floydhub.com/rpilarcz/datasets/pascal-voc-2007/1/JPEGImages" --annotation "https://www.floydhub.com/rpilarcz/datasets/pascal-voc-2007/1/Annotations" --savepb --gpu 0.8
from darkflow.net.build import TFNet
import cv2
from ctypes import *
import math
import random
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
from PIL import Image
import numpy as np
import random
import time
import subprocess

def return_random():
    for x in xrange(10):
        return random.randint(1,101)

def grab_frame(cap):
    ret,frame = cap.read()
    return cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)

#Initatite Tensor Flow
classes = {
    'train':'#88eac8',
    'stop sign':'#fc1d14',
    'car':'#4f5885',
    'traffic light':'#fc1d14',
    'bus':'#68c067',
    'truck':'#68c067',
    'person':'#ffffff',
    'bicycle':'#68c067',
    'bird':'#887b0d',
    'cat':'#887b0d',
    'dog':'#887b0d',
    'horse':'#887b0d',
    'sheep':'#887b0d',
    'cow':'#887b0d'
    }

# options = {"model": "cfg/yolo.cfg", "load": "cfg/yolo.weights", "threshold": 0.25, "gpu": 0.8}
options = {"pbLoad": "built_graph/yolo.pb", "metaLoad": "built_graph/yolo.meta", "threshold": 0.25, "gpu": 0.8}

tfnet = TFNet(options)

shouldSave = False
axisOff = True
total_vehicles = 0
#Initiate the two cameras
cap = cv2.VideoCapture('video.mp4')
fps = cap.get(cv2.CAP_PROP_FPS)
print(fps)
num_frames = 120;

print ("Capturing {0} frames".format(num_frames))

# Start time
start = time.time()

# Grab a few frames
for i in xrange(0, num_frames):
    ret, frame = cap.read()

# End time
end = time.time()

# Time elapsed
seconds = end - start
print("Time taken : {0} seconds".format(seconds))

# Calculate frames per second
fps = num_frames / seconds;
print("Estimated frames per second : {0}".format(fps))

c = 2
while True:

    #create two subplots
    # ax = plt.subplot(1,1,1)
    # ax.set_title("IAX RI | Total Vehicles: "+str(total_vehicles),fontsize=9)
    total_vehicles = 0
    # if(axisOff):
    #     ax.set_axis_off()
    #create two image plots
    # im1 = ax.imshow(grab_frame(cap))

    # plt.ion()
    start = time.time()
    resultJSON = tfnet.return_predict(grab_frame(cap))
    end = time.time()
    print("Time taken : {0} seconds".format(end-start))

    rect = ""
    text = ""
    #Detect Objects
    for x in resultJSON:
        label = x['label']
        x1 = x['topleft']['x']
        y1 = x['topleft']['y']
        x2 = x['bottomright']['x']
        y2 = x['bottomright']['y']
        print (label,x1,y1,x2,y2)
    #     label_width = x1-x2
    #     label_height = y1-y2
    #     if(classes.has_key(label)):
    #         if(label=="car" or label=="truck" or label=="bicycle" or label=="bus"):
    #             total_vehicles = total_vehicles+1
    #         col = classes[label]
    #         rect = patches.Rectangle((x1-(label_width),y1-(label_height)),label_width,label_height,linestyle='solid',fill=False,edgecolor=col,linewidth=1,facecolor='none')
    #         tex = ax.text((x1-label_width)+5,(y1-label_height)-16,label,size='smaller',bbox=dict(pad=2,edgecolor=col,color=col))
    #         ax.add_patch(rect)
    #         # polygon = patches.Polygon([(750,600), (250,1000), (1750,1000), (1150,600)],color='red', fc=(0,0,0,0.8), ec=(0,0,0,1), lw=1)
    #         # ax.add_patch(polygon)
    #
    # im1.set_data(grab_frame(cap))
    # # plt.pause(0.
    # figName = 'output5/prediction'+str(c)+'.png';
    # plt.savefig(figName,dpi=100)
    # c = c+1
    # # plt.show()
    # ax.cla()

    # print c

#save to video
if(shouldSave):
    command = "ffmpeg -framerate 13 -i output/prediction%00d.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p output/iax_output.mp4";
    proc = subprocess.Popen(command,stdout=subprocess.PIPE, shell=True)
    output = proc.stdout.read()
    print ("-------------")
    print (output)

plt.ioff() # due to infinite loop, this gets never called.