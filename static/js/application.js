
$(document).ready(function(){
    //connect to the socket server.
    var socket = io.connect('http://' + document.domain + ':' + location.port + '/test');
    var numbers_received = [];

    //receive details from server
    socket.on('newnumber', function(msg) {

        console.log("Received number > " + msg.number);
        $("#log").attr("src","static/output1/prediction"+msg.number+".png");
    });

});