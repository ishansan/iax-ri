from darkflow.net.build import TFNet
import cv2
from ctypes import *
import math
import random
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from PIL import Image
import numpy as np

img = "./sample_img/cars/1.jpg"
classes = {
    'train':'#88eac8',
    'stop sign':'#fc1d14',
    'car':'#4f5885',
    'traffic light':'#fc1d14',
    'bus':'#68c067',
    'truck':'#68c067',
    'person':'#ffffff',
    'bicycle':'#68c067',
    'bird':'#887b0d',
    'cat':'#887b0d',
    'dog':'#887b0d',
    'horse':'#887b0d',
    'sheep':'#887b0d',
    'cow':'#887b0d'
    }

options = {"model": "cfg/yolo.cfg", "load": "bin/yolo.weights", "threshold": 0.25, "gpu": 0.6}

tfnet = TFNet(options)

imgcv = cv2.imread(img)

resultJSON = tfnet.return_predict(imgcv)

#Detecting objects from video input
im2 = np.array(Image.open(img), dtype=np.uint8)
fig,ax = plt.subplots(1)
ax.set_axis_off()
fig.add_axes(ax)
ax.imshow(im2)
for x in resultJSON:
	label = x['label']
	x1 = x['topleft']['x']
	y1 = x['topleft']['y']
	x2 = x['bottomright']['x']
	y2 = x['bottomright']['y']
	print label,x1,y1,x2,y2
	label_width = x1-x2
	label_height = y1-y2
	if(classes.has_key(label)):
		col = classes[label]
		rect = patches.Rectangle((x1-(label_width),y1-(label_height)),label_width,label_height,linestyle='solid',fill=False,edgecolor=col,linewidth=1,facecolor='none')
		ax.text((x1-label_width)+5,(y1-label_height)-16,label,size='smaller',bbox=dict(pad=2,edgecolor=col,color=col))
		ax.add_patch(rect)

plt.show()

#Calculate real-time densities and burst time

#Send to scheduling algorithm to simulate (1 Lane)