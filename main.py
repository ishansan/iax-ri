#!/usr/bin/python
# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use('Agg')
from darkflow.net.build import TFNet
import os.path
import cv2
from ctypes import *
import math
import random
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
from PIL import Image
import numpy as np
import random
import subprocess
from flask.ext.socketio import SocketIO, emit
from flask import Flask, render_template, url_for, \
    copy_current_request_context
from random import random
from time import sleep
from threading import Thread, Event

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
app.config['DEBUG'] = True

socketio = SocketIO(app)

thread = Thread()
thread_stop_event = Event()


def grab_frame(cap):
    (ret, frame) = cap.read()
    return cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)


classes = {
    'train': '#88eac8',
    'stop sign': '#fc1d14',
    'car': '#4f5885',
    'traffic light': '#fc1d14',
    'bus': '#68c067',
    'truck': '#68c067',
    'person': '#ffffff',
    'bicycle': '#68c067',
    'bird': '#887b0d',
    'cat': '#887b0d',
    'dog': '#887b0d',
    'horse': '#887b0d',
    'sheep': '#887b0d',
    'cow': '#887b0d',
    }

options = {
    'model': 'cfg/yolo.cfg',
    'load': 'cfg/yolo.weights',
    'threshold': 0.25,
    'gpu': 0.6,
    }

shouldSave = False
axisOff = True
total_vehicles = 0
cap = cv2.VideoCapture('video.mp4')
c = 2
tfnet = ''


class Detection(Thread):

    def __init__(self):
        self.delay = 3
        super(Detection, self).__init__()

    def emitStream(self):
        global total_vehicles
        global c
        global tfnet
        tfnet = TFNet(options)
        while not thread_stop_event.isSet():
            ax = plt.subplot(1, 1, 1)
            ax.set_title('IAX RI | Total Vehicles: '
                         + str(total_vehicles), fontsize=9)
            total_vehicles = 0
            if axisOff:
                ax.set_axis_off()

                im1 = ax.imshow(grab_frame(cap))
                plt.ion()

                resultJSON = tfnet.return_predict(grab_frame(cap))

                rect = ''

                text = ''
                for x in resultJSON:
                    label = x['label']
                    x1 = x['topleft']['x']
                    y1 = x['topleft']['y']
                    x2 = x['bottomright']['x']
                    y2 = x['bottomright']['y']
                #print label, x1, y1, x2, y2
                label_width = x1 - x2
                label_height = y1 - y2
                if classes.has_key(label):
                    if label == 'car' or label == 'truck' or label \
                        == 'bicycle' or label == 'bus':
                        total_vehicles = total_vehicles + 1
                    col = classes[label]
                    rect = patches.Rectangle(
                        (x1 - label_width, y1 - label_height),
                        label_width,
                        label_height,
                        linestyle='solid',
                        fill=False,
                        edgecolor=col,
                        linewidth=1,
                        facecolor='none',
                        )
                    tex = ax.text(x1 - label_width + 5, y1
                                  - label_height - 16, label,
                                  size='smaller', bbox=dict(pad=2,
                                  edgecolor=col, color=col))
                    ax.add_patch(rect)

                    # polygon = patches.Polygon([(750,600), (250,1000), (1750,1000), (1150,600)],color='red', fc=(0,0,0,0.8), ec=(0,0,0,1), lw=1)
                    # ax.add_patch(polygon)

                im1.set_data(grab_frame(cap))
                plt.pause(0.1)
                figName = 'static/output1/prediction' + str(c) + '.png'
                plt.savefig(figName, dpi=100)
                c = c + 1
                plt.show()
                ax.cla()
                print 'prediction', c
                sleep(self.delay)
                socketio.emit('newnumber', {'number': c},
                              namespace='/test')

    def run(self):
        self.emitStream()


@app.route('/')
def index():
    return render_template('index.html')


@socketio.on('connect', namespace='/test')
def test_connect():
    global thread
    print 'Client connected'
    if not thread.isAlive():
        print 'Starting Thread'
        #thread = Detection()
        #thread.start()


@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print 'Client disconnected'

if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', port=5000)